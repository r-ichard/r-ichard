---
thumbnail: dreem_async.png
project_title: "Asynchronous workflows"
sub_title: "Orchestration tool for asynchronous tasks in patient creation flow"
tldr: "In order to synchronize correctly patient information through microservices and external tools I created an orchestration tool to handle failure and concurrency."
skills: 
 - Web development
 - Team management
 - Project management
challenges:
 - Keeping data synced in multiple internal + external services
 - Handling failure and retry mechanism properly
 - Locking tables to avoid concurrency errors 
---

My mission was initially to optimize the process of sending mails and creating patients when they arrive at the clinic.

As I dove deeper into the project, I realized that in order to avoid latency and ensure optimal results, 
I needed to implement asynchronous tasks. 
However, managing these tasks would require orchestration and efficient failure management.

To address this challenge, I utilized my web development and project management skills and used Python and Celery to create an orchestration tool.

I focused on handling the challenges of keeping data synced in multiple internal and external services, handling failure and retry mechanisms properly, locking tables to avoid concurrency errors.

The result was an efficient and reliable tool that enabled us to synchronize patient information across different systems seamlessly. 