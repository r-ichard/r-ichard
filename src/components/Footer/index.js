import * as React from "react"
import {
    footer,
    colorFiller,
    upLink
} from "./style.module.scss"
import "@fontsource/roboto-mono/700.css"


class Footer extends React.Component {
    render() {

        return (
            <footer className={footer}>

                <section>
                    <div>
                        <a href="https://www.linkedin.com/in/r-ichard/" className="default-color" target="_blank" rel="noreferrer">linkedin</a><strong className="default-color">_</strong>
                        <a href="https://github.com/r-ichard" className="default-color" target="_blank" rel="noreferrer">github</a><strong className="default-color">_</strong>
                        <a href="https://twitter.com/richard0r" className="default-color" target="_blank" rel="noreferrer">twitter</a>
                    </div>
                    <a href="#" className={upLink}>
                        <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.68215 11.0879L11.4111 1.35889L20.8321 11.0879" stroke="#5485B6" strokeWidth="2"
                                  strokeLinecap="round" strokeLinejoin="round"/>
                        </svg>
                        <span>Up</span>
                    </a>
                </section>
                <div className={colorFiller}>

                </div>
            </footer>
        )
    }
}

export default Footer
