import * as React from "react"
import {bigSubTitle, blue, row, title, ctaLink} from "../Layout/layout.module.scss"
import {
    checkboxes,
    contactSection,
    inputGroup,
} from "./style.module.scss"
import "@fontsource/roboto-mono/700.css"
import {useForm, ValidationError} from '@formspree/react';


const Contact = () => {
    const [state, handleSubmit] = useForm("xvodvkbq");
    const ContactForm = () => {
        if (state.succeeded) {
            return <p>Thanks for writing ! I'm getting back to you !</p>;
        }
        return (<form onSubmit={handleSubmit}>
            <div className={checkboxes}>
                <div>
                    <input type="checkbox" id="web_dev" name="web_dev"/>
                    <label htmlFor="web_dev">Web development</label>
                </div>
                <div>
                    <input type="checkbox" id="team_management" name="team_management"/>
                    <label htmlFor="team_management">Team management</label>
                </div>
                <div>
                    <input type="checkbox" id="architecture" name="architecture"/>
                    <label htmlFor="architecture">Architecture</label>
                </div>
                <div>
                    <input type="checkbox" id="business_analysis" name="business_analysis"/>
                    <label htmlFor="business_analysis">Business analysis</label>
                </div>
                <div>
                    <input type="checkbox" id="audit" name="audit"/>
                    <label htmlFor="audit">Audit</label>
                </div>
                <div>
                    <input type="checkbox" id="project_management" name="project_management"/>
                    <label htmlFor="project_management">Project management</label>
                </div>
                <div>
                    <input type="checkbox" id="other" name="other"/>
                    <label htmlFor="other">Other</label>
                </div>
            </div>
            <div className={row}>
                <div className={inputGroup}>
                    <input type="text" placeholder="Your full name" name="full_name" required="required"/>
                    <ValidationError
                        prefix="Email"
                        field="email"
                        errors={state.errors}
                    />
                    <input type="email" placeholder="Your email" name="email" required="required"/>

                    <ValidationError
                        prefix="Message"
                        field="message"
                        errors={state.errors}
                    />
                    <textarea placeholder="What can I do for you?" name="message" maxLength="1000" minLength="5" required="required"/>

                </div>
                <button type="submit" className={ctaLink} disabled={state.submitting}>Send message</button>
            </div>
        </form>)
    }
    return (
        <section id="contact" className={`${row} ${contactSection}`}>
            <h2 className={title}><strong>_Contact</strong></h2>
            <div className={bigSubTitle}><p>Tell me what you <span className={blue}>need</span></p></div>

            <ContactForm/>
        </section>
    )
}
export default Contact
