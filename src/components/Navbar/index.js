import * as React from "react"
import {computer, mobile} from "../Layout/layout.module.scss"
import {
    leftElements,
    mobileMenu,
    opened,
    closed,
    menu,
    menuToggle,
    menuContent,
    mobileNavBar,
    computerMenu
} from "./style.module.scss"
import Burger from '@animated-burgers/burger-slip'
import ReactTooltip from "react-tooltip"
import {ThemeToggler} from "gatsby-plugin-dark-mode"
import Logo from "./logo"
import {useState} from "react";
import downloadFile from '../../files/resume.pdf'

const Navbar = () => {
    const [mobileMenuIsOpen, setIsOpen] = useState(false);
    const [currentTheme, setCurrentTheme] = useState(null)
    const MobileLink = (props) => {
        return ( <a href={props.href} className="default-color" onClick={() => {
                        setIsOpen(!mobileMenuIsOpen)
                    }}>
                        {props.text}
                    </a>
        )
    }
    const CustomThemeSwitch = (props) => {
        return (
            <ThemeToggler>
                {({theme, toggleTheme}) => {
                    // Don't render anything at compile time. Deferring rendering until we
                    // know which theme to use on the client avoids incorrect initial
                    // state being displayed.
                    if (theme == null) {
                        return null
                    }

                    let switchTo = theme === "dark" ? "light" : "dark"
                    return (
                        <>
                            <button
                                className={`circle-border ${theme}`}
                                tabIndex={0}
                                data-tip={`Switch to ${switchTo} mode`}
                                onClick={(e) => {
                                    setCurrentTheme(theme)
                                    toggleTheme(switchTo)
                                }}
                            >
                                <div className="circle">&nbsp;</div>
                            </button>
                            <ReactTooltip place="left" type={theme} effect="solid"/>
                        </>
                    )
                }}
            </ThemeToggler>
        )
    }
    return (
        <nav>
            <Logo/>
            <div className={`${computer} ${computerMenu}`}>
                <div className={`${leftElements}`}>
                    <a href="#projects">Projects</a>
                    <a href="#about">About</a>
                    <a href="#contact">Contact</a>
                    <a href={downloadFile} download>Download resume</a>
                </div>
                <div>
                        <CustomThemeSwitch/>
                </div>
            </div>
            <div className={`${mobile} ${mobileMenu}`}>
                <div className={mobileNavBar}>
                    <div className={menuToggle}>
                        <Burger Component="button" type="button" isOpen={mobileMenuIsOpen} onClick={() => {
                            setIsOpen(!mobileMenuIsOpen)
                        }}/>
                    </div>
                </div>

                <div className={`${menu} default-bg ${mobileMenuIsOpen ? opened : closed}`}>

                    <div className={`${menuContent}`}>
                        <CustomThemeSwitch/>
                        <MobileLink href="#projects" text="Projects" />
                        <MobileLink href="#about" text="About" />
                        <MobileLink href="#contact" text="Contact" />
                        <MobileLink href="/download" text="Download resume" />
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Navbar
