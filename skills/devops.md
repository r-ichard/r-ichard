---
name: "DevOps"
level: 2
---
I started learning DevOps 2 years after I became a developer.
Today, I am proficient in creating every DevOps tool that my daily work requires.
I set up several CI/CD environments, git-hooks, and other Makefile tools at Conity.
