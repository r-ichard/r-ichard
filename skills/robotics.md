---
name: "Robotics"
level: 4
---

I am amazed by the scope of possibilities offered by robots to improve our industry and our health.
Thus, through a regular technological watch on the emerging innovations of this field, I feed my natural curiosity and guide my studies to identify the new trends in robotics.

Working in what is my long-standing hobby would be a new and rewarding experience for me.


