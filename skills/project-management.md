---
name: "Project management"
level: 2
---
I believe project management is mastered only in a professional context.
As a CTO at Conity, I managed and organized project follow-ups, mitigated risks, and dealt with crises with both internal and external teams.

However, each project is unique, and I feel like there will always be something new to learn throughout my whole career.
