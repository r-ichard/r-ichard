---
name: "AI | Machine Learning"
level: 4
---

I love how AI and machine learning can solve complex business issues and science problems. Therefore, it is a field that I am particularly interested in.
I am always fascinated by innovations coming out of this industry.  

I haven’t had the opportunity to work with AI and/or machine learning yet. However, that is a project I hope to add to my career experience soon
