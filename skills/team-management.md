---
name: "Team management"
level: 2
---
One of the most rewarding aspects of my career has been working with people to achieve common goals.

I'm excited about taking on a more leadership role and giving people the opportunity to grow and cooperate in a way that benefits everyone.
