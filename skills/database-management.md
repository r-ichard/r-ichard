---
name: "Database management"
level: 3
---
I've been working with databases for more than 8 years. My work always required designing and using databases daily.

I’ve worked with many SQL and rational databases (MariaDB, PostgreSQL), and I also had the chance to work with MongoDB.
