---
name: "Cloud Architecture"
level: 2
---
Cloud architecture is a self-taught skill. However, the fundamental knowledge I have acquired during my CTO positions allowed me to build efficient and robust cloud architecture.

I mainly worked with AWS services, and I also have basic knowledge of Google Cloud’s platform solution.
