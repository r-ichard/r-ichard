---
name: "Business analysis"
level: 2
---
Solving business needs by identifying key performance levers and optimizing processes is
very satisfying to me. As a result, I've always been vigilant of my work’s impact on the businesses I work with.
