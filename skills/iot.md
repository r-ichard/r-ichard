---
name: "Embedded | IoT"
level: 4
---
Embedded software development and IoT is another passion of mine. I mostly read and play with this skill (I own several microcontrollers at home).

I never really had the chance to work professionally on these domains, but it would be thrilling to put the skill to good use.
