## Get started

### Requirements:
- npm / yarn
- Node 18

1. Run `yarn install`
2. Run `yarn run gatsby develop`
3. Once you are done please check everything builds for production `yarn run gatsby build && yarn run gatsby serve` 